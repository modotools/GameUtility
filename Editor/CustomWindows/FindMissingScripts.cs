using UnityEditor;
using UnityEngine;

namespace _Editor.CustomWindows
{
    public class FindMissingScriptsRecursively : EditorWindow
    {
        static int m_goCount, m_componentsCount, m_missingCount;

        [MenuItem("Window/FindMissingScriptsRecursively")]
        public static void ShowWindow()
        {
            GetWindow(typeof(FindMissingScriptsRecursively));
        }

        public void OnGUI()
        {
            if (GUILayout.Button("Find Missing Scripts in selected GameObjects")) FindInSelected();
        }

        static void FindInSelected()
        {
            var go = Selection.gameObjects;
            m_goCount = 0;
            m_componentsCount = 0;
            m_missingCount = 0;
            foreach (var g in go) FindInGo(g);
            Debug.Log($"Searched {m_goCount} GameObjects, {m_componentsCount} components, found {m_missingCount} missing");
        }

        static void FindInGo(GameObject g)
        {
            m_goCount++;
            var components = g.GetComponents<Component>();
            for (var i = 0; i < components.Length; i++)
            {
                m_componentsCount++;
                if (components[i] != null)
                    continue;
                m_missingCount++;
                var s = g.name;
                var t = g.transform;
                while (t.parent != null)
                {
                    var parent = t.parent;
                    s = parent.name + "/" + s;
                    t = parent;
                }

                Debug.Log(s + " has an empty script attached in position: " + i, g);
            }

            // Now recurse through each child GO (if there are any):
            foreach (Transform childT in g.transform)
                //Debug.Log("Searching " + childT.name  + " " );
                FindInGo(childT.gameObject);
        }
    }
}