using System.Collections.Generic;
using System.IO;
using System.Linq;
using Core.Editor.Tools;
using Core.Editor.Utility.AnimationHelper;
using Core.Extensions;
using Core.Unity.Interface;
using UnityEditor;
using UnityEngine;
using AssetOperations = Core.Editor.Utility.AssetOperations;

namespace _Editor.CustomWindows
{
    public class DeveloperTools : EditorWindow
    {
        Object m_markedAsset;
        Vector2 m_scrollPos;

        [MenuItem("Tools/DeveloperTools")]
        static void Open() => GetWindow<DeveloperTools>();

        void OnGUI()
        {
            using (var scrollScope = new EditorGUILayout.ScrollViewScope(m_scrollPos))
            {
                m_scrollPos = scrollScope.scrollPosition;
                GameObjectSelectedGUI();
                AssetSelectedGUI();
            }
        }

        void AssetSelectedGUI()
        {
            GUILayout.Label("Marked Asset: ");
            m_markedAsset = EditorGUILayout.ObjectField(m_markedAsset, typeof(Object), false);

            var selectedAssets = Selection.objects;
            if (selectedAssets.IsNullOrEmpty())
                return;

            if (selectedAssets.Length == 1
                && selectedAssets[0] != null
                && AssetDatabase.IsMainAsset(selectedAssets[0]))
            {
                if (GUILayout.Button($"Mark Selected Asset: { selectedAssets[0].name }"))
                    m_markedAsset = selectedAssets[0];

                //if (selectedAssets[0] is AnimatorController anim)
                //    AnimatorControllerOptions(anim);
            }

            GUILayout.Label("Selection: ");
            foreach (var asset in selectedAssets)
                GUILayout.Label(asset.name);

            FixMainAssetParentTool(selectedAssets);
            ReparentingTool(selectedAssets);
            UnparentingTool(selectedAssets);
            RemoveTool(selectedAssets);

            //FixGraph(Selection.activeObject);
            DependencyViewer.I.DrawGUI(Selection.activeObject);
        }
        //static void FixGraph(Object activeObject)
        //{
        //    if (!GUILayout.Button("Fix Graph"))
        //        return;
        //    var allAssets = AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(activeObject));
        //    foreach (var asset in allAssets)
        //    {
        //        if (_!(asset is INodeGraph nodeGraph))
        //            continue;
        //        nodeGraph.NodeObjs = nodeGraph.Parent.ChildAssets.Where(ca => (ca is INode n) && n.Graph == nodeGraph)
        //            .ToArray();
        //        EditorUtility.SetDirty(asset);
        //    }
        //}
        static void FixMainAssetParentTool(IEnumerable<Object> selectedAssets)
        {
            if (!GUILayout.Button("Fix main asset"))
                return;
            foreach (var asset in selectedAssets.OfType<IScriptableObject>())
                AssetOperations.FixParentOfAsset(asset);
        }

        static void RemoveTool(IEnumerable<Object> selectedAssets)
        {
            if (!GUILayout.Button("Remove selected Objects"))
                return;
            foreach (var asset in selectedAssets)
            {
                if (AssetDatabase.IsMainAsset(asset))
                    continue;
                AssetDatabase.RemoveObjectFromAsset(asset);
            }
            AssetDatabase.SaveAssets();
        }

        static void UnparentingTool(IEnumerable<Object> selectedAssets)
        {
            if (!GUILayout.Button("Unparent selected Objects"))
                return;
            foreach (var asset in selectedAssets)
            {
                if (AssetDatabase.IsMainAsset(asset))
                    continue;
                //Debug.Log(AssetDatabase.GetAssetPath(asset));
                var path = Path.GetDirectoryName(AssetDatabase.GetAssetPath(asset)) + "/" + asset.name + ".asset";
                var clone = Instantiate(asset);
                clone.name = asset.name;
                AssetDatabase.CreateAsset(clone, path);
                //AssetDatabase.RemoveObjectFromAsset(asset);
            }
            // todo 1: remap references of the copy then delete original
            AssetDatabase.SaveAssets();
        }

        void ReparentingTool(IEnumerable<Object> selectedAssets)
        {
            if (m_markedAsset == null || !AssetDatabase.IsMainAsset(m_markedAsset) ||
                !GUILayout.Button("Reparent selected Objects")) return;

            foreach (var asset in selectedAssets)
            {
                if (!AssetDatabase.IsMainAsset(asset))
                    continue;
                var clone = Instantiate(asset);
                clone.name = asset.name;
                AssetDatabase.AddObjectToAsset(clone, m_markedAsset);
            }
            // todo 1: remap references of the copy then delete original
            AssetDatabase.SaveAssets();
        }

        static void GameObjectSelectedGUI()
        {
            if (Selection.activeGameObject == null)
                return;

            GUILayout.Label("Selection.activeGameObject: " + Selection.activeGameObject);

            if (GUILayout.Button("Make Avatar"))
                AvatarMaker.MakeAvatar(Selection.activeGameObject);

            if (GUILayout.Button("Make Avatar Mask"))
                AvatarMaker.MakeAvatarMask(Selection.gameObjects);
        }
    }
}