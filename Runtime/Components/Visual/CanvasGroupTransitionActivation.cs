﻿using System;
using Core.Unity.Interface;
using UnityEngine;
using UnityEngine.Events;

namespace GameUtility.Components.Visual
{
    public class CanvasGroupTransitionActivation : MonoBehaviour, ITransitionActivation
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] CanvasGroup m_canvasGroup;

        [SerializeField] UnityEvent m_onActiveTransitionDone;
        [SerializeField] UnityEvent m_onDeactivateTransitionDone;
        [SerializeField] bool m_initActive;
        [SerializeField] float m_transitionSpeed=1f;
#pragma warning restore 0649 // wrong warnings for SerializeField

        bool m_initialized;

        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public InactiveFollowup InactiveFollowup { get; set; }
        public bool IsInTransition => Math.Abs(m_canvasGroup.alpha - TargetAlpha) >= float.Epsilon;
        public bool IsActiveOrActivating { get; private set; }

        float TargetAlpha => IsActiveOrActivating ? 1f : 0f;
        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter

        void Awake()
        {
            if (!m_initialized)
                InitActive(m_initActive);
        }

        // ReSharper disable once FlagArgument
        public virtual void InitActive(bool active)
        {
            //Debug.Log($"InitActive {gameObject.name}{GetInstanceID()} {active} \n{Environment.StackTrace}");
            m_initialized = true;

            m_canvasGroup.alpha = active ? 1f : 0f;
            gameObject.SetActive(active);
            IsActiveOrActivating = active;
        }

        public void SetActive(bool active)
        {
            //Debug.Log($"SetActive {gameObject.name}{GetInstanceID()} {active} \n{Environment.StackTrace}");

            if (active)
                gameObject.SetActive(true);

            IsActiveOrActivating = active;

            //Debug.Log($"canvas - scene {gameObject.scene.name} m_isActive {m_isActive} IsActiveOrActivating {IsActiveOrActivating}");
        }

        void Update()
        {
            if (!IsInTransition)
                return;
            var dir = IsActiveOrActivating ? 1f : -1f;
            var alpha = Mathf.Clamp01(m_canvasGroup.alpha + dir * m_transitionSpeed * Time.unscaledDeltaTime);

            m_canvasGroup.alpha = alpha;
            //Debug.Log($"canvas - scene {gameObject.scene.name} update alpha {alpha}");

            if (Math.Abs(alpha - TargetAlpha) > float.Epsilon)
                return;
            //Debug.Log($"canvas - scene {gameObject.scene.name} done update!");

            if (IsActiveOrActivating)
                m_onActiveTransitionDone.Invoke();
            else
                m_onDeactivateTransitionDone.Invoke();

            if (!IsActiveOrActivating && this.FollowUp())
                return;
            
            gameObject.SetActive(IsActiveOrActivating);
        }
    }
}
