﻿using Core.Unity.Interface;
using UnityEngine;
using UnityEngine.Events;

namespace GameUtility.Components.Visual
{
    public class ActionDurationTransitionActivation : MonoBehaviour, ITransitionActivation
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] UnityEvent m_onActive;
        [SerializeField] UnityEvent m_onDeactivate;

        [SerializeField] bool m_initActive;
        [SerializeField] float m_transitionDuration = 1f;
#pragma warning restore 0649 // wrong warnings for SerializeField

        bool m_initialized;
        float m_timer;

        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public InactiveFollowup InactiveFollowup { get; set; }
        public bool IsInTransition => m_timer > 0f;
        public bool IsActiveOrActivating { get; private set; }
        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter

        void Awake()
        {
            if (!m_initialized)
                InitActive(m_initActive);
        }

        // ReSharper disable once FlagArgument
        public void InitActive(bool active)
        {
            m_initialized = true;
            gameObject.SetActive(active);
            IsActiveOrActivating = active;
        }

        public void SetActive(bool active)
        {
            if (active)
                gameObject.SetActive(true);

            if (active != IsActiveOrActivating)
            {
                IsActiveOrActivating = active;
                m_timer = m_transitionDuration;
            }

            if (active)
                m_onActive.Invoke();
            else 
                m_onDeactivate.Invoke();
        }

        void Update()
        {
            if (!IsInTransition)
                return;

            m_timer -= Time.deltaTime;

            if (IsInTransition)
                return;

            // if (IsActiveOrActivating)
            //     m_onActiveTransitionDone.Invoke();
            // else
            //     m_onDeactivateTransitionDone.Invoke();

            if (!IsActiveOrActivating && this.FollowUp())
                return;
            
            gameObject.SetActive(IsActiveOrActivating);
        }
    }
}
