using System.Collections;
using UnityEngine;

namespace GameUtility.Components.Visual
{
    public class PulsatingSprite : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField

        [SerializeField] float m_minAlpha = 0.5f;
        [SerializeField] float m_maxAlpha = 1.5f;

        [SerializeField] float m_frequency = 1.0f;

        [SerializeField]
        SpriteRenderer m_sprite;
#pragma warning restore 0649 // wrong warnings for SerializeField

        float m_current;

        // Start is called before the first frame update
        void Start()
        {
            if (m_sprite == null)
                m_sprite = GetComponent<SpriteRenderer>();

            StartCoroutine(PulsatingRoutine());
        }

        IEnumerator PulsatingRoutine()
        {
            while (true)
            {
                while (m_current < 1.0f)
                {
                    m_current = Mathf.Clamp01(m_current + Time.deltaTime * m_frequency);

                    UpdateAlpha();
                    yield return new WaitForEndOfFrame();
                }
                while (m_current > 0.0f)
                {
                    m_current = Mathf.Clamp01(m_current - Time.deltaTime * m_frequency);

                    UpdateAlpha();
                    yield return new WaitForEndOfFrame();
                }
            }
            // ReSharper disable once IteratorNeverReturns
        }

        void UpdateAlpha()
        {
            if (m_sprite == null)
                return;
            var c = Mathf.SmoothStep(0.0f, 1.0f, m_current);
            var col = m_sprite.color;
            col.a = Mathf.Lerp(m_minAlpha, m_maxAlpha, c);
            m_sprite.color = col;
        }
    }
}
