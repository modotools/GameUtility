﻿using System.Collections.Generic;
using Core.Unity.Extensions;
using Core.Unity.Types;
using Core.Unity.Utility.PoolAttendant;
using UnityEngine;
using UnityEngine.Serialization;

namespace GameUtility.Components
{
    // ReSharper disable once IdentifierTypo
    public class PartsDespawner : MonoBehaviour
    {
        struct PartsDespawnInfo
        {
            public float Seconds;
            public GameObject Part;
            //public Renderer Renderer;
        }
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] float m_minSeconds;
        [SerializeField] float m_maxSeconds;

        [SerializeField] PrefabData m_despawnEffect;
#pragma warning restore 0649 // wrong warnings for SerializeField

        float m_time;
        readonly List<PartsDespawnInfo> m_parts = new List<PartsDespawnInfo>();

        void OnEnable()
        {
            m_time = 0;
            m_parts.Clear();

            var rends = GetComponentsInChildren<Renderer>(true);
            foreach (var r in rends)
            {
                var go = r.gameObject;

                go.SetActive(true);
                m_parts.Add(new PartsDespawnInfo()
                {
                    Part = go,
                    //Renderer = r,
                    Seconds = Random.Range(m_minSeconds, m_maxSeconds),
                });
            }
        }

        void OnDisable() => m_time = 0;

        void Update()
        {
            m_time += Time.deltaTime;
            for (var i =  m_parts.Count-1; i >= 0; i--)
            {
                if (m_time < m_parts[i].Seconds)
                    continue;
                DespawnPartAt(i);
            }
            if (m_time > m_maxSeconds)
                Despawn();
        }

        void DespawnPartAt(int i)
        {
            var despawnPrefab = m_despawnEffect.Prefab;

            if (despawnPrefab != null)
                despawnPrefab.GetPooledInstance(m_parts[i].Part.transform.position);
            m_parts[i].Part.SetActive(false);
            m_parts.RemoveAt(i);
        }

        void Despawn()
        {
            if (!enabled)
                return;

            gameObject.TryDespawnOrDestroy();
            m_time = -float.MaxValue;
        }
    }
}