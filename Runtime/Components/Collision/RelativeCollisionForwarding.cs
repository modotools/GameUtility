using UnityEngine;

namespace GameUtility.Components.Collision
{
    /// <inheritdoc />
    /// <summary>
    /// Use this if you have a RelativeCollider anyway and you want the AssociatedObject to handle the collision
    /// </summary>
    [RequireComponent(typeof(RelativeCollider))]
    public class RelativeCollisionForwarding : MonoBehaviour
    {
        GameObject AssociatedObject => m_relativeCollider.AssociatedObject;

        RelativeCollider m_relativeCollider;
        void Start() => m_relativeCollider = GetComponent<RelativeCollider>();
        
        void OnCollisionEnter(UnityEngine.Collision other)
        {
            var go = RelativeCollider.GetRoot(other);

            var handling = AssociatedObject.GetComponents<ICollisionObjEnterHandling>();
            foreach (var handler in handling)
                handler.OnCollisionObjectEnter(go, other);
        }
        void OnCollisionStay(UnityEngine.Collision other)
        {
            var go = RelativeCollider.GetRoot(other);

            var handling = AssociatedObject.GetComponents<ICollisionObjStayHandling>();
            foreach (var handler in handling)
                handler.OnCollisionObjectStay(go, other);
        }
        void OnCollisionExit(UnityEngine.Collision other)
        {
            var go = RelativeCollider.GetRoot(other);

            var handling = AssociatedObject.GetComponents<ICollisionObjExitHandling>();
            foreach (var handler in handling)
                handler.OnCollisionObjectExit(go, other);
        }
    }

    public interface ICollisionObjEnterHandling { void OnCollisionObjectEnter(GameObject collidedWith, UnityEngine.Collision data); }
    public interface ICollisionObjExitHandling { void OnCollisionObjectExit(GameObject collidedWith, UnityEngine.Collision data); }
    public interface ICollisionObjStayHandling { void OnCollisionObjectStay(GameObject collidedWith, UnityEngine.Collision data); }
    public interface ICollisionObjEnterExitHandling : ICollisionObjEnterHandling, ICollisionObjExitHandling { }
    public interface ICollisionObjHandling : ICollisionObjEnterHandling, ICollisionObjStayHandling, ICollisionObjExitHandling { }
}
