using UnityEngine;

namespace GameUtility.Components.Collision
{
    /// <inheritdoc />
    /// <summary>
    /// Use this if you have a RelativeCollider anyway and you want the AssociatedObject to handle the trigger
    /// </summary>
    [RequireComponent(typeof(RelativeCollider))]
    public class RelativeTriggerForwarding : MonoBehaviour
    {
        RelativeCollider m_relativeCollider;
        GameObject AssociatedObject => m_relativeCollider.AssociatedObject;

        void Start() => m_relativeCollider = GetComponent<RelativeCollider>();

        void OnTriggerEnter(Collider other)
        {
            if (m_relativeCollider == null)
                return;

            var go = RelativeCollider.GetRoot(other);

            var handling = AssociatedObject.GetComponents<ITriggerObjEnterHandling>();
            foreach (var handler in handling)
                handler.OnTriggerObjectEnter(go);
        }

        void OnTriggerStay(Collider other)
        {
            if (m_relativeCollider == null)
                return;

            var go = RelativeCollider.GetRoot(other);

            var handling = AssociatedObject.GetComponents<ITriggerObjStayHandling>();
            foreach (var handler in handling)
                handler.OnTriggerObjectStay(go);
        }

        void OnTriggerExit(Collider other)
        {
            if (m_relativeCollider == null)
                return;

            var go = RelativeCollider.GetRoot(other);
            var handling = AssociatedObject.GetComponents<ITriggerObjExitHandling>();
            foreach (var handler in handling)
                handler.OnTriggerObjectExit(go);
        }

        void OnValidate()
        {
            if (!TryGetComponent<RelativeCollider>(out _))
                Debug.LogError($"{nameof(RelativeTriggerForwarding)}: Missing {nameof(RelativeCollider)} on {gameObject}");
        }
    }
    public interface ITriggerObjEnterHandling { void OnTriggerObjectEnter(GameObject root); }
    public interface ITriggerObjExitHandling { void OnTriggerObjectExit(GameObject root); }
    public interface ITriggerObjStayHandling { void OnTriggerObjectStay(GameObject root); }
    public interface ITriggerObjEnterExitHandling : ITriggerObjEnterHandling, ITriggerObjExitHandling { }
    public interface ITriggerObjHandling : ITriggerObjEnterHandling, ITriggerObjStayHandling, ITriggerObjExitHandling { }
}
