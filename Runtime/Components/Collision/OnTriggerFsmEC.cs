using Core.Unity.Types;
using Core.Unity.Types.Attribute;
using Core.Unity.Types.Fsm;
using Core.Unity.Types.Fsm.Obsolete;
using UnityEngine;
using UnityEngine.Serialization;

namespace GameUtility.Components.Collision
{
    /// <summary>
    /// Simple forwarding of a trigger-event to Playmaker when trigger-object has script of type interface (given by scriptFilter)
    /// SK.: I think filtering for one ClassTypeReference is enough for now, if we have the use-case we should think about Filter-List with AND, OR,...
    /// [EC] = Event Component
    /// </summary>
    public class OnTriggerFsmEC : MonoBehaviour
    {
        [ClassTypeConstraint(AllowRegular = false, AllowInterface = true)]
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] ClassTypeReference m_scriptFilter;

        [SerializeField] FSMGameObjectEventRef m_onTriggerEnterFSM;

        [SerializeField] FSMGameObjectEventRef m_onTriggerStayFSM;

        [SerializeField] FSMGameObjectEventRef m_onTriggerExitFSM;
#pragma warning restore 0649 // wrong warnings for SerializeField

        void OnTriggerEnter(Collider other)
        {
            var go = RelativeCollider.GetRoot(other);
            if (!go.TryGetComponent(m_scriptFilter.Type, out _))
                return;

            m_onTriggerEnterFSM.Invoke(go);
        }

        void OnTriggerStay(Collider other)
        {
            var go = RelativeCollider.GetRoot(other);

            if (!go.TryGetComponent(m_scriptFilter.Type, out _))
                return;

            m_onTriggerStayFSM.Invoke(go);
        }

        void OnTriggerExit(Collider other)
        {
            var go = RelativeCollider.GetRoot(other);

            if (!go.TryGetComponent(m_scriptFilter.Type, out _))
                return;

            m_onTriggerExitFSM.Invoke(go);
        }
    }
}