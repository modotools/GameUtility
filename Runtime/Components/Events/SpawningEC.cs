﻿using Core.Interface;
using UnityEngine;
using UnityEngine.Events;

namespace GameUtility.Components.Events
{
    /// <summary>
    /// Unity Event once GameObject is spawned via pool
    /// [EC] = Event Component
    /// </summary>
    public class SpawningEC : MonoBehaviour, IPoolable
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] UnityEvent m_onSpawn;
        [SerializeField] UnityEvent m_onDespawn;
#pragma warning restore 0649 // wrong warnings for SerializeField
        public void OnSpawn() => m_onSpawn.Invoke();
        public void OnDespawn() => m_onDespawn.Invoke();
    }
}
