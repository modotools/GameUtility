﻿using Core.Unity.Types;
using UnityEngine;

namespace GameUtility.Components.Debugging
{
    public class AnimatorStateSelectorBehaviour : MonoBehaviour
    {
        [SerializeField, AnimTypeDrawer]
        AnimatorStateRef m_anim;

        // Update is called once per frame
        void Update()
        {
            var animator = m_anim.Animator;
            if (animator == null)
                return;

            var current = animator.GetCurrentAnimatorStateInfo(0).shortNameHash;
            if (current == m_anim.StateHash)
                return;

            m_anim.Animator.Play(m_anim);
        }
    }
}
