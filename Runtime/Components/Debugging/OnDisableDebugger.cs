﻿using System;
using UnityEngine;

namespace GameUtility.Components.Debugging
{
    public class OnDisableDebugger : MonoBehaviour
    {
        void OnDisable()
        {
            Debug.Log($"OnDisable {gameObject.name} {Environment.StackTrace}");
        }
    }
}
