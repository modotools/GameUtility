
using System.Collections.Generic;
using GameUtility.Coroutines;
using GameUtility.Data.Visual;
using UnityEngine;

namespace GameUtility.Components.Actions
{
    /// <summary>
    /// Action for flickering by exchanging material
    /// [AC] = Action Component
    /// </summary>
    public class FlickerMaterialAC : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] FlickerMaterialData m_flickerData;
        [SerializeField] bool m_revertOnDisable;

        [SerializeField] GameObject m_flickerModel;
#pragma warning restore 0649 // wrong warnings for SerializeField
        IMemorizeMaterials m_memoryComp;
        readonly List<MaterialMemory> m_memory = new List<MaterialMemory>();
        List<MaterialMemory> Memory => m_memoryComp != null ? m_memoryComp.Memory : m_memory;

        void Awake()
        {
            m_memoryComp = GetComponent<IMemorizeMaterials>();

            if (m_flickerModel == null)
                return;

            var duration = m_flickerData.FlickerDuration;
            var frequency = m_flickerData.Frequency;
            var matMap = m_flickerData.Override;
            m_flickerData = new FlickerMaterialData()
            {
                Renderer = m_flickerModel.GetComponentsInChildren<Renderer>(),
                FlickerDuration = duration,
                Frequency = frequency,
                Override = matMap
            };
        }

        Coroutine m_routine;
        public void StartFlicker()
        {
            if (m_routine != null)
                StopCoroutine(m_routine);
            m_routine = StartCoroutine(FlickerRoutine.Flicker(m_flickerData, Memory));
        }

        public void StopFlicker()
        {
            if (m_routine != null)
                StopCoroutine(m_routine);
            MaterialExchange.Revert(Memory);
        }

        public void SetModel(GameObject model) =>
            m_flickerModel = model;
    }
}