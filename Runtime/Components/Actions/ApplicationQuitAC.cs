using UnityEngine;

namespace GameUtility.Components.Actions
{
    /// <summary>
    /// Action for quitting application
    /// [AC] = Action Component
    /// </summary>
    public class ApplicationQuitAC : MonoBehaviour
    {
        public void Quit() {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
        }
    }
}
