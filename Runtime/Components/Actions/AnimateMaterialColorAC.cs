﻿using System.Collections.Generic;
using Core.Extensions;
using GameUtility.Coroutines;
using GameUtility.Data.Visual;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Serialization;

namespace GameUtility.Components.Actions
{
    /// <summary>
    /// Action for animating material color
    /// [AC] = Action Component
    /// </summary>
    public class AnimateMaterialColorAC : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] MaterialColorAnimData m_colorizeMap;

        [SerializeField] 
        bool m_restoreMemoryOnDisable;
#pragma warning restore 0649 // wrong warnings for SerializeField

        bool m_colorized;

        IMemorizeMaterials m_memoryComp;
        readonly List<MaterialMemory> m_memory = new List<MaterialMemory>();
        List<MaterialMemory> Memory => m_memoryComp != null ? m_memoryComp.Memory : m_memory;
        
        MaterialColorFromToAnimData m_fromToMap;

        void Awake()
        {
            m_memoryComp = GetComponent<IMemorizeMaterials>();
            InitFromToMap();
        }
        void InitFromToMap()
        {
            if (m_colorizeMap.ColorMapData.MaterialColorMap.IsNullOrEmpty())
            {
                Debug.LogError("No MaterialColorMap setup!");
                return;
            }

            m_fromToMap = new MaterialColorFromToAnimData()
            {
                Duration = m_colorizeMap.Duration,
                Revert = m_colorizeMap.Revert,
                Renderer = m_colorizeMap.ColorMapData.Renderer,
                ColorMapData = new MaterialFromToColorMap[m_colorizeMap.ColorMapData.MaterialColorMap.Length]
            };
        }

        Coroutine m_animateMaterialCoroutine;

        [UsedImplicitly]
        public void Colorize()
        {
            m_colorized = true;
            m_animateMaterialCoroutine = StartCoroutine(AnimateMaterialColorRoutine.Animate(m_colorizeMap, m_fromToMap, Memory));
        }

        [UsedImplicitly]
        public void Revert()
        {
            if (!m_colorized)
                return;
            m_colorized = false;
            if (m_animateMaterialCoroutine!= null)
                StopCoroutine(m_animateMaterialCoroutine);
            MaterialExchange.Revert(Memory);
            Memory.Clear();
        }

        void OnDisable()
        {
            if (m_colorized && m_restoreMemoryOnDisable)
                Revert();
        }
    }
}
