﻿using JetBrains.Annotations;
using UnityEngine;

namespace GameUtility.Components.Actions
{
    /// <summary>
    /// Action for stopping wwise event
    /// [AC] = Action Component
    /// </summary>
    public class StopWwiseEventAC : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] AkEvent m_event;
        [SerializeField] int m_transitionDuration;

        [SerializeField] AkCurveInterpolation m_curve;
#pragma warning restore 0649 // wrong warnings for SerializeField

        [UsedImplicitly]
        public void Stop() => m_event.Stop(m_transitionDuration, m_curve);
    }
}
