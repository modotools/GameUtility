﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace GameUtility.Components.Actions
{
    /// <summary>
    /// Action for destroying components
    /// [AC] = Action Component
    /// </summary>
    public class DestroyComponentsAC : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] List<Component> m_destroyComponents;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public void DoDestroy()
        {
            foreach (var c in m_destroyComponents)
                Destroy(c);
        }

        public void Clear() => m_destroyComponents.Clear();
    }
}
