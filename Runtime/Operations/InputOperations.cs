﻿//#define LOG_INPUT

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace GameUtility.Operations
{
    public static class InputOperations
    {
#if LOG_INPUT
        static readonly Dictionary<InputAction, List<Action<InputAction.CallbackContext>>> k_allInputs 
            = new Dictionary<InputAction, List<Action<InputAction.CallbackContext>>>();
#endif
        public static void Add(this InputAction action, Action<InputAction.CallbackContext> call)
        {
#if LOG_INPUT
            if (!k_allInputs.ContainsKey(action))
                k_allInputs.Add(action, new List<Action<InputAction.CallbackContext>>());

            k_allInputs[action].Add(call);
            Debug.Log($"Add {action.name} Callback: {call.Target}.{call.Method.Name}");
#endif
            action.performed += call;
        }

        public static void Remove(this InputAction action, Action<InputAction.CallbackContext> call)
        {
#if LOG_INPUT
            if (k_allInputs.ContainsKey(action))
            {
                k_allInputs[action].Remove(call);
                if (k_allInputs[action].Count <= 0)
                    k_allInputs.Remove(action);
            }

            Debug.Log($"Remove {action.name} Callback: {call.Target}.{call.Method.Name}");
#endif
            action.performed -= call;
        }

        public static void LogAllInputs()
        {
#if LOG_INPUT
            var allInputsLog = "All existing Inputs:\n";
            foreach (var input in k_allInputs)
            {
                allInputsLog += $"Input {input.Key.name} Calls {input.Value.Count}\n";

                foreach (var call in input.Value)
                    allInputsLog += $"{call.Target} {call.Method.Name}";
            }
            Debug.Log(allInputsLog);
#endif
        }

    }
}
