﻿using System;
using Core.Unity.Types;
using UnityEngine.Events;

namespace GameUtility.Energy
{
    public interface IEnergyComponent
    {        
        // ReSharper disable UnusedMemberInSuper.Global
        EnergyID EnergyType { get; }
        float Energy { get; }
        float MaxEnergy { get; }
        bool IsInvulnerable { get; }

        bool CanUse(float amount);
        // ReSharper disable once MethodNameNotMeaningful
        /// <summary>
        /// Friendly energy removal
        /// </summary>
        void Use(float amount);

        void SetInvulnerable(float time);
        /// <summary>
        /// violent energy removal
        /// </summary>
        void Damage(float amount);

        void Regenerate(float amount);
        void Deplete();

        void Reset();
        void SetRegenerationBlocked(bool blocked);

        void AddChangeListener(UnityAction<float> action);
        void RemoveChangeListener(UnityAction<float> action);
        // ReSharper restore UnusedMemberInSuper.Global
    }

    [Serializable]
    public class RefEnergyComponent : InterfaceContainer<IEnergyComponent> { }
}
