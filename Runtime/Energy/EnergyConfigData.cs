﻿using System;

namespace GameUtility.Energy
{
    [Serializable]
    public struct EnergyConfigData
    {
        public float MaxEnergy;
        public float InvulnerableTimeAfterDamage;
        public float TimeBeforeRecharging;
        public float RechargeRate;
        public float InitEnergyLevelPercent;
        public bool CanOvershoot;
        public bool DiscreteValues;

        public static EnergyConfigData Default => new EnergyConfigData()
        {
            MaxEnergy = 3,
            InvulnerableTimeAfterDamage = 0.1f,
            TimeBeforeRecharging = 0f,
            RechargeRate = 0f,
            InitEnergyLevelPercent = 1f,
            CanOvershoot = false,
            DiscreteValues = false
        };
    }
}