﻿using Core.Unity.Interface;

namespace GameUtility.Data.EntityAnima
{
    public struct TargetPriorityData
    {
        public int Priority;
        public ITarget Target;
    }
}