using Core.Unity.Attributes;
using Core.Unity.Types.ID;

namespace GameUtility.Data.Visual
{
    [EditorIcon("icon-id")]
    public class FX_ID : IDAsset { }
}