﻿using UnityEngine;

namespace GameUtility.Data.PhysicalConfrontation
{
    public interface ICollisionMaterialComponent
    {
        Vector3 CollisionPos { get; }
        Quaternion CollisionRotation { get; }

        float CollisionPosCenterPercent { get; }
        CollisionMaterialID Material { get; }
    }
}
