﻿namespace GameUtility.Data.PhysicalConfrontation
{
    public interface IHitHandler : ICollisionMaterialComponent
    {
        void OnHit();
    }
}
