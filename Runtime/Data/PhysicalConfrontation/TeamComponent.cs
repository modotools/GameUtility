﻿using UnityEngine;

namespace GameUtility.Data.PhysicalConfrontation
{
    public class TeamComponent : MonoBehaviour, ITeamComponent
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] TeamFlagID m_teamID;
#pragma warning restore 0649 // wrong warnings for SerializeField

        // ReSharper disable once ConvertToAutoProperty
        public TeamFlagID TeamFlag
        {
            get => m_teamID;
            set => m_teamID = value;
        }

        public int TeamID => m_teamID.GetInstanceID();

        public bool IsOnSameTeam(ITeamComponent teamComponent) => teamComponent.TeamID == TeamID;
    }

    public interface ITeamComponent
    {
        int TeamID { get; }
        bool IsOnSameTeam(ITeamComponent teamComponent);
    }
}
