﻿using System;
using System.Collections;
using GameUtility.Data.TransformAttachment;
using UnityEngine;

namespace GameUtility.Data.PhysicalConfrontation
{
    public static class KnockBackRoutine
    {
        public struct KnockBackData
        {
            public IMutableTransformData Body;
            public Vector3 Direction;
            public float KnockDelay;
            public float KnockDuration;
            public float KnockFactor;
            public AnimationCurve ForceCurve;

            public float RecoverDuration;
            public float MaxRecoverFactor;
            public AnimationCurve RecoverCurve;
        }

        [Serializable]
        public struct KnockBackConfigData
        {
            public float KnockDelay;
            public float KnockDuration;
            public AnimationCurve ForceCurve;
            public float RecoverDuration;
            public float MaxRecoverFactor;
            public AnimationCurve RecoverCurve;
        }

        public static KnockBackData GetKnockBackData(KnockBackConfigData configData, IMutableTransformData body)
            => new KnockBackData()
            {
                Body = body,
                KnockDelay = configData.KnockDelay,
                KnockDuration = configData.KnockDuration,
                ForceCurve = configData.ForceCurve,
                RecoverDuration = configData.RecoverDuration,
                MaxRecoverFactor = configData.MaxRecoverFactor,
                RecoverCurve = configData.RecoverCurve,
            };

        /// <summary>
        /// Coroutine used to make renderer flicker (when actor is hurt for example).
        /// </summary>
        public static IEnumerator KnockBack(KnockBackData data)
        {
            if (data.Body == null)
                yield break;

            var body = data.Body;
            var time = 0f;
            if (data.KnockDelay > 0f)
                yield return new WaitForSeconds(data.KnockDelay);

            yield return new WaitForFixedUpdate();
            while (time < data.KnockDuration)
            {
                time += Time.deltaTime;
                var i = Mathf.Clamp01(time/data.KnockDuration);
                var force = data.ForceCurve.Evaluate(i);
                var velocity = data.KnockFactor * Mathf.Clamp01(force);
                body.Position += data.Direction * (velocity * Time.fixedDeltaTime);
                yield return new WaitForFixedUpdate();
            }

            time = 0f;
            while (time < data.RecoverDuration)
            {
                time += Time.deltaTime;
                var i = Mathf.Clamp01(time / data.RecoverDuration);
                var rec = data.RecoverCurve.Evaluate(i);
                var velocity = Mathf.Min(data.KnockFactor, data.MaxRecoverFactor) * Mathf.Clamp01(rec);
                body.Position += -data.Direction * (velocity * Time.fixedDeltaTime);
                yield return new WaitForFixedUpdate();
            }
            //body.Velocity = Vector3.zero;
            
        }
    }
}