using System;
using UnityEngine.InputSystem;

namespace GameUtility.Data.Input
{
    public struct ActionMapping
    {
        public InputActionReference ActionRef;
        public Action<InputAction.CallbackContext> Call;

        public Action<InputAction> LinkAction;
        public Action<InputAction> UnlinkAction;
    }
}