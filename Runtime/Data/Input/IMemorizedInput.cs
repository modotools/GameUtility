
namespace GameUtility.Data.Input
{
    public interface IMemorizedInput
    {
        string Name { get; }

        bool CanInvoke { get; }
        void Invoke();
    }
}