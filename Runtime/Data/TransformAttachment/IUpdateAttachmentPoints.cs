using System;
using Core.Unity.Types;

namespace GameUtility.Data.TransformAttachment
{
    public interface IUpdateAttachmentPoints
    {
        void UpdateAttachmentPoints(AttachmentPointsProvider prov);
    }

    
    [Serializable]
    public class RefIUpdateAttachmentPoints : InterfaceContainer<IUpdateAttachmentPoints> { }
}