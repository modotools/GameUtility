﻿using System.Collections.Generic;
using UnityEngine;

namespace GameUtility.Data.TransformAttachment
{
    public class AttachmentPointsComponent : MonoBehaviour
    {
#pragma warning disable 0649
        [SerializeField] AttachmentPoint[] m_attachmentPoints;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public void Extract(List<AttachmentPoint> attachmentPoints)
        {
            foreach (var ap in m_attachmentPoints)
            {
                if (IsContained(attachmentPoints, ap))
                {
                    Debug.LogWarning($"Attachment point with ID {ap.ID} is already contained in list!");
                    continue;
                }
                attachmentPoints.Add(ap);
            }
        }

        static bool IsContained(List<AttachmentPoint> attachmentPoints, AttachmentPoint ap) 
            => attachmentPoints.FindIndex(a => a.ID == ap.ID) != -1;
    }
}
