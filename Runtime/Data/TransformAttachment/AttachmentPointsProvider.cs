﻿using System.Collections.Generic;
using UnityEngine;

namespace GameUtility.Data.TransformAttachment
{
    public class AttachmentPointsProvider : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] List<RefIUpdateAttachmentPoints> m_attachmentPointsUpdater;
#pragma warning restore 0649 // wrong warnings for SerializeField


        readonly List<AttachmentPoint> m_attachmentPoints = new List<AttachmentPoint>();
        readonly List<AttachmentPointsComponent> m_attachmentPointsComponents = new List<AttachmentPointsComponent>();

        public void AddAttachmentPoints(AttachmentPointsComponent aps)
        {
            if (m_attachmentPointsComponents.Contains(aps))
                return;
            m_attachmentPointsComponents.Add(aps);

            ReinitializeAttachmentPoints();
            UpdateListener();
        }

        public void RemoveAttachmentPoints(AttachmentPointsComponent aps)
        {
            m_attachmentPointsComponents.Remove(aps);
            ReinitializeAttachmentPoints();
            UpdateListener();
        }

        void ReinitializeAttachmentPoints()
        {
            m_attachmentPoints.Clear();
            foreach (var apc in m_attachmentPointsComponents)
                apc.Extract(m_attachmentPoints);
        }

        void UpdateListener()
        {
            foreach (var u in m_attachmentPointsUpdater) 
                u.Result?.UpdateAttachmentPoints(this);
        }

        public TransformData GetAttachment(AttachmentID id) => m_attachmentPoints.Find(p => p.ID == id).TransformData;
    }
}
