using UnityEngine;

namespace GameUtility.Interface
{
    public interface IWeapon
    {
        GameObject Actor { get; }
        float WeaponLength { get; }
    }

    public interface IWeaponSpawn
    {
        void Initialize(IWeapon w);
    }
}