using System.Collections.Generic;
using GameUtility.Data.PhysicalConfrontation;

namespace GameUtility.Interface
{
    public interface IAttackDataProvider
    {
        IReadOnlyList<IWeapon> GetCurrentWeapons();
        AttackData GetCurrentAttackData { get; }
        AttackData GetNextAttackData { get; }
    }
}