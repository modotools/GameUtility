using System;
using Core.Unity.Types;
using GameUtility.Data.PhysicalConfrontation;
using UnityEngine;

namespace GameUtility.Interface
{
    public interface IShield
    {
        GameObject Actor { get; }
        CollisionMaterialID CollisionMaterial { get; }
    }

    [Serializable]
    public class RefIShield : InterfaceContainer<IShield>{}
}