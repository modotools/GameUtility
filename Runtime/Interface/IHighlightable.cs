namespace GameUtility.Interface
{
    public interface IHighlightable
    {
        void EnableHighlight();
        void DisableHighlight();
    }
}
