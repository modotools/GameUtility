﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace GameUtility.Interface
{        
    // ReSharper disable UnusedMemberInSuper.Global
    public interface IControlCentre
    {        
        GameObject Model { get; }
        Animator Animator { get; }

        //AttachmentPointsComponent Attachments { get;}
        GameObject ControlledObject { get; }
        //Rigidbody ControlledRigidBody { get; }

        PlayerInput PlayerInput { get; }
        bool Blocked { get; }
        Vector3 Position { get; }
        Vector2 V2Position { get; }

        void InitializeModel(GameObject model);
        //void InitializeWithController(PlayerInput input);
        //void RemoveControl();
    }    
    // ReSharper restore UnusedMemberInSuper.Global
}
