﻿using System;
using System.Collections.Generic;
using Core.Unity.Utility.Debug;
using GameUtility.Data.Debugging;
using UnityEngine;

namespace GameUtility.Config
{
    [CreateAssetMenu(menuName = "Game/DebugDrawConfig")]
    public class DebugDrawConfig : ScriptableObject
    {
        public static DebugDrawConfig Instance { get; set; }
        public static DebugDrawConfig I => Instance;

        [Serializable]
        public struct DrawPositionSetting
        {
            public DebugDrawID TagID;
            public bool Enabled;
            public Color DrawColor;
            public float DrawSize;
        }

        [Serializable]
        public struct DrawLineSetting
        {
            public DebugDrawID TagID;
            public bool Enabled;
            public Color DrawColor;
        }

        [Serializable]
        public struct DrawTextSetting
        {
            public DebugDrawID TagID;
            public bool Enabled;

            public Color TextColor;
            public Vector2 Offset;
        }
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] bool m_enabled;

        [SerializeField] DrawPositionSetting[] m_posSettings;
        [SerializeField] DrawLineSetting[] m_lineSettings;
        [SerializeField] DrawTextSetting[] m_textSettings;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public void DrawPosition(Vector3 pos, DebugDrawID id)
        {
            if (!m_enabled)
                return;

            var setting = Array.Find(m_posSettings, p => p.TagID == id);
            DrawPosition(pos, setting);
        }

        public void DrawPath(IList<Vector3> path, DebugDrawID id)
        {
            if (!m_enabled)
                return;

            var setting = Array.Find(m_lineSettings, p => p.TagID == id);
            DrawPath(path, setting);
        }

        public void DrawLine(Vector3 start, Vector3 end, DebugDrawID id)
        {
            if (!m_enabled)
                return;

            var setting = Array.Find(m_lineSettings, p => p.TagID == id);
            DrawLine(start, end, setting);
        }
        public void DebugDrawText(Vector2 screenPos, string text, DebugDrawID id)
        {
            if (!m_enabled)
                return;

            var setting = Array.Find(m_textSettings, p => p.TagID == id);
            DebugDrawText(screenPos, text, setting);
        }

        static void DebugDrawText(Vector2 screenPos, string text, DrawTextSetting setting)
        {
            if (!setting.Enabled)
                return;

            CustomDebugDraw.DrawText(screenPos + setting.Offset, text, setting.TextColor);
        }

        static void DrawPosition(Vector3 pos, DrawPositionSetting setting)
        {
            if (!setting.Enabled)
                return;
            CustomDebugDraw.DrawPosition(pos, setting.DrawColor, setting.DrawSize);
        }

        static void DrawPath(IList<Vector3> path, DrawLineSetting setting)
        {
            if (!setting.Enabled)
                return;
            for (var i = 0; i < path.Count - 1; i++)
                Debug.DrawLine(path[i], path[i + 1], setting.DrawColor);
        }

        static void DrawLine(Vector3 start, Vector3 end, DrawLineSetting setting)
        {
            if (!setting.Enabled)
                return;
            Debug.DrawLine(start, end, setting.DrawColor);
        }
    }
}
