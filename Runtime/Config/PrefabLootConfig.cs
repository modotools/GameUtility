using System;
using System.Collections.Generic;
using Core.Unity.Types;
using GameUtility.Data.TransformAttachment;
using GameUtility.PooledEventSpawning;
using UnityEngine;
using Random = UnityEngine.Random;

namespace GameUtility.Config
{
    [Serializable]
    [CreateAssetMenu(menuName = "Game/PrefabLootConfig")]
    public class PrefabLootConfig : ScriptableObject
    {
        [Serializable]
        public struct LootData
        {
            [SerializeField]
            public PrefabData Loot;
            [SerializeField]
            public int Min;
            public int Max;
        }

        public List<LootData> Rewards;

        public void GetLoot(ref List<GameObject> lootList, TransformData spawnLocation)
        {
            foreach (var reward in Rewards)
            {
                if (reward.Loot.Prefab == null)
                    continue;
                var amount = Random.Range(reward.Min, reward.Max);
                for (var i = 0; i < amount; i++)
                {
                    var loot = reward.Loot.Prefab.PooledSpawn(spawnLocation.Position, spawnLocation.Rotation);
                    lootList.Add(loot);
                }
            }
        }

        public void GetLootPrefabs(ref List<GameObject> lootPrefabList)
        {
            foreach (var reward in Rewards)
            {
                if (reward.Loot.Prefab == null)
                    continue;
                var amount = Random.Range(reward.Min, reward.Max);
                for (var i = 0; i < amount; i++) 
                    lootPrefabList.Add(reward.Loot.Prefab);
            }
        }
    }
}