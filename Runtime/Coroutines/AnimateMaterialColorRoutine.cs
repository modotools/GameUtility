﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Types;
using GameUtility.Data.Visual;
using UnityEngine;

namespace GameUtility.Coroutines
{
    //[Serializable]
    //public struct MaterialColorAnimMap
    //{
    //    public Material Material;
    //    public string ColorKey;
    //    public Color FromColor;
    //    public Color ToColor;
    //}

    [Serializable]
    public struct MaterialColorAnimData
    {
        public MaterialColorizeData ColorMapData;
        public float Duration;
        public bool Revert;
    }

    [Serializable]
    public struct MaterialColorFromToAnimData
    {
        public MaterialFromToColorMap[] ColorMapData;
        public Renderer[] Renderer;

        public float Duration;
        public bool Revert;
    }

    [Serializable]
    public struct MaterialColorAnimData2
    {
        public MaterialColorizeData2 ColorMapData;
        public float Duration;
    }
    public static class AnimateMaterialColorRoutine
    {
        public static IEnumerator Animate(MaterialColorAnimData2 data, 
            GameObject model, 
            MaterialColorFromToAnimData fromToMap, 
            List<MaterialMemory> memory)
        {
            if (model == null)
                yield break;
            var r = model.GetComponentsInChildren<Renderer>();
            yield return Animate(
            new MaterialColorAnimData()
            {
                Duration = data.Duration,
                ColorMapData = new MaterialColorizeData()
                {
                    Renderer = r,
                    MaterialColorMap = data.ColorMapData.MaterialColorMap
                }
            }, 
            fromToMap,
            memory);
        }

        /// <summary>
        /// Coroutine used to animate material color.
        /// </summary>
        public static IEnumerator Animate(MaterialColorAnimData data, MaterialColorFromToAnimData fromToMap, List<MaterialMemory> memory)
        {
            if (data.ColorMapData.Renderer == null)
                yield break;
            if (data.ColorMapData.MaterialColorMap == null)
                yield break;
            MaterialColorize.PrepareColorize(data.ColorMapData, memory, fromToMap);
            yield return Animate(fromToMap, memory);
        }

        static IEnumerator Animate(MaterialColorFromToAnimData fromToMap, IEnumerable<MaterialMemory> memory)
        {
            if (fromToMap.Renderer == null)
                yield break;
            if (fromToMap.ColorMapData == null)
                yield break;
            
            var timer = 0f;
            while (timer < fromToMap.Duration)
            {
                timer += Time.deltaTime;
                foreach (var p in fromToMap.ColorMapData)
                {
                    if (p.Material == null)
                        continue;
                    var c = Color.Lerp(p.FromColor, p.ToColor, timer / fromToMap.Duration);
                    p.Material.SetColor(p.ColorKey, c);
                }
                yield return null;
            }

            if (fromToMap.Revert)
                MaterialExchange.Revert(memory);
        }
    }
}