﻿using Core.Unity.Interface;
using Core.Unity.Utility.PoolAttendant;
using UnityEngine;

namespace GameUtility.PooledEventSpawning
{
    public static class EventSpawning
    {
        // ReSharper disable UnusedMember.Global, MemberCanBePrivate.Global
        static GameObject TriggerAndReturn(this GameObject original, GameObject spawnedObj)
        {
            if (!spawnedObj.TryGetComponent<IPrefabInstanceMarker>(out var instanceMarker))
                instanceMarker = spawnedObj.AddComponent<PrefabInstanceMarkerComponent>();
            instanceMarker.Prefab = original;
            PrefabSpawnedEvent.Trigger(spawnedObj, original);
            return spawnedObj;
        }

        public static GameObject Spawn(this GameObject original, Vector3 position, Quaternion rotation)
        {
            var obj = Object.Instantiate(original, position, rotation);
            return TriggerAndReturn(original, obj);
        }
        public static GameObject PooledSpawn(this GameObject original, Vector3 position, Quaternion rotation)
        {
            var obj = original.GetPooledInstance(position, rotation);
            return TriggerAndReturn(original, obj);
        }
        public static GameObject PooledSpawn(this GameObject original, Vector3 position, Quaternion rotation, string name)
        {
            var obj = original.GetPooledInstance(position, rotation, name);
            return TriggerAndReturn(original, obj);
        }
        public static GameObject Spawn(this GameObject original, Vector3 position, Quaternion rotation, Transform parent)
        {
            var obj = Object.Instantiate(original, position, rotation, parent);
            return TriggerAndReturn(original, obj);
        }

        public static GameObject PooledSpawn(this GameObject original, Vector3 position, Quaternion rotation, Transform parent)
        {
            var obj = original.GetPooledInstance(position, rotation);
            obj.transform.SetParent(parent);
            return TriggerAndReturn(original, obj);
        }

        public static GameObject Spawn(this GameObject original)
        {
            var obj = Object.Instantiate(original);
            return TriggerAndReturn(original, obj);
        }
        public static GameObject PooledSpawn(this GameObject original)
        {
            var obj = original.GetPooledInstance();
            return TriggerAndReturn(original, obj);
        }

        public static GameObject Spawn(this GameObject original, Transform parent) => original.Spawn(parent, false);

        public static GameObject PooledSpawn(this GameObject original, Transform parent) =>
            original.PooledSpawn(parent.position, parent.rotation, parent);

        public static GameObject Spawn(this GameObject original, Transform parent, bool instantiateInWorldSpace)
        {
            var obj = Object.Instantiate(original, parent, instantiateInWorldSpace);
            return TriggerAndReturn(original, obj);
        }
        // ReSharper restore UnusedMember.Global, MemberCanBePrivate.Global
    }
}