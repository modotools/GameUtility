﻿using UnityEngine;
using Random = UnityEngine.Random;

namespace GameUtility.Feature.Exploding
{
    public class ExplodingPartMemory : MonoBehaviour
    {
        public Vector3 MemorizedLocalPosition;
        public Quaternion MemorizedLocalRotation;
        public Vector3 MemorizedLocalScale;

        public Transform MemorizedParent;
        public bool HadRigidbody;

        public void Memorize()
        {
            var thisTr = transform;
            MemorizedLocalPosition = thisTr.localPosition;
            MemorizedLocalRotation = thisTr.localRotation;
            MemorizedLocalScale = thisTr.localScale;

            MemorizedParent = thisTr.parent;
            HadRigidbody = thisTr.TryGetComponent<Rigidbody>(out _);
        }

        public void ApplyExplosion(Vector3 pos, Vector3 posRandomization,
            float force, float radius, float upwardsMod)
        {
            if (gameObject == null)
                return;
            transform.SetParent(null, true);

            if (!TryGetComponent<Rigidbody>(out var rigidBody))
                rigidBody = gameObject.AddComponent<Rigidbody>();
            if (rigidBody == null)
                return;

            pos.x += Random.Range(-posRandomization.x, posRandomization.x);
            pos.y += Random.Range(-posRandomization.y, posRandomization.y);
            pos.z += Random.Range(-posRandomization.z, posRandomization.z);

            // rigidBody.useGravity = false;
            rigidBody.AddExplosionForce(force, pos, radius, upwardsMod, ForceMode.Impulse);
        }

        public void DestroyRigidbody()
        {
            if (!gameObject.TryGetComponent<Rigidbody>(out var rigid))
                return;
            Destroy(rigid);
        }

        public void Restore()
        {
            if (this == null)
                return;

            var thisTr = transform;
            thisTr.SetParent(MemorizedParent, true);
            //yield return null;
            thisTr.localPosition = MemorizedLocalPosition;
            thisTr.localRotation = MemorizedLocalRotation;
            thisTr.localScale = MemorizedLocalScale;

            Destroy(this);
        }
    }
}
