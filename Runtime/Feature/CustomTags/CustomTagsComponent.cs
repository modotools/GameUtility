﻿using System.Collections.Generic;
using UnityEngine;

namespace GameUtility.Feature.CustomTags
{
    /// <summary>
    /// Component to use multiple custom Tags for various uses
    /// </summary>
    public class CustomTagsComponent : MonoBehaviour
    {
        public List<CustomTagID> CustomTags = new List<CustomTagID>();
    }
}
