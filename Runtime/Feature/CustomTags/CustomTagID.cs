﻿using Core.Unity.Attributes;
using Core.Unity.Types.ID;

namespace GameUtility.Feature.CustomTags
{
    [EditorIcon("icon-id")]
    public class CustomTagID : IDAsset { }
}
