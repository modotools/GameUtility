﻿using Core.Unity.Extensions;
using GameUtility.Components.Visual;

namespace GameUtility.Feature.Loading
{
    public class LoadingCanvas : CanvasGroupTransitionActivation
    {
        public static LoadingCanvas Instance;

        //public static bool Active => Instance!= null && Instance.IsActiveOrActivating;

        public override void InitActive(bool active)
        {      
            if (Instance != null && Instance != this)
            {
                Instance.InitActive(active);
                gameObject.DestroyEx();
                return;
            }   

            base.InitActive(active);
             
            var go = gameObject;
            Instance = this;
            DontDestroyOnLoad(go);
        }
    }
}
