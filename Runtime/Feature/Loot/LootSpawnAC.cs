﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Types;
using Core.Unity.Attributes;
using GameUtility.Config;
using GameUtility.Data.TransformAttachment;
using GameUtility.InitSystem;
using GameUtility.PooledEventSpawning;
using UnityEngine;

namespace GameUtility.Feature.Loot
{
    public class LootSpawnAC : MonoBehaviour, IInitDataComponent<LootSpawnAC.ConfigData>
    {
        [Serializable]
        public struct ConfigData
        {
            public List<WeightedLoot> SpawnData;
            //public static ConfigData Default = new ConfigData() {SpawnData = new List<WeightedLoot>()};
        }

        [Serializable]
        public struct WeightedLoot
        {
            public PrefabLootConfig Loot;
            public int Weight;
        }

#pragma warning disable 0649 // wrong warnings for SerializeField
        [CopyPaste]
        [SerializeField] ConfigData m_data;

        [SerializeField] protected TransformData m_transformData;
#pragma warning restore 0649 // wrong warnings for SerializeField
        // ReSharper disable once ConvertToAutoPropertyWithPrivateSetter
        public void GetData(out ConfigData data) => data = m_data;
        public void InitializeWithData(ConfigData data) => m_data = data;
        
        public void Spawn()
        {
            if (m_transformData.Parent == null)
                m_transformData.Parent = transform;
            var weightSum = m_data.SpawnData.Sum(s => s.Weight);
            var spawnAtWeight = UnityEngine.Random.Range(0, weightSum);

            var currentWeight = 0;
            using (var goListScoped = SimplePool<List<GameObject>>.I.GetScoped())
            {
                var lootPrefabs = goListScoped.Obj;
                foreach (var s in m_data.SpawnData)
                {
                    currentWeight += s.Weight;
                    if (spawnAtWeight >= currentWeight) 
                        continue;
                    // No loot!
                    if (s.Loot == null)
                        break;
                    s.Loot.GetLootPrefabs(ref lootPrefabs);
                    break;
                }

                SpawnLoot(ref lootPrefabs);
            }
        }

        protected virtual void SpawnLoot(ref List<GameObject> lootPrefabs)
        {
            foreach (var prefab in lootPrefabs) 
                prefab.PooledSpawn(m_transformData.Position, m_transformData.Rotation);
        }
    }
}