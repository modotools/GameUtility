using System.Collections.Generic;
using Core.Unity.Types;
using Core.Unity.Utility.PoolAttendant;
using GameUtility.Components.Actions;
using GameUtility.Components.Events;
using GameUtility.Data.TransformAttachment;
using GameUtility.PooledEventSpawning;
using UnityEngine;
using UnityEngine.Serialization;

namespace GameUtility.Feature.Loot
{
    public class LootSpawnShootOutAC : LootSpawnAC
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] PrefabData m_lootContainer;
        [SerializeField] Vector2 m_spawnForce = Vector2.up;
        [SerializeField] Vector2 m_spawnPosOffset;
#pragma warning restore 0649 // wrong warnings for SerializeField
        protected override void SpawnLoot(ref List<GameObject> lootPrefabs)
        {
            foreach (var prefab in lootPrefabs)
            {
                GetSpawnForce(out var force, out var offset);
                var td = new TransformData()
                {
                    Parent = m_transformData.Parent,
                    LocalPosition = m_transformData.LocalPosition + offset,
                    LocalRotation = m_transformData.LocalRotation
                };
                var spawned = prefab.PooledSpawn(td.Position, td.Rotation);
                OnSpawned(spawned, force);
            }
        }


        void OnSpawned(GameObject spawned, Vector3 force)
        {
            var pos = spawned.transform.position;

            // we instantiate a container for spawnedObject, that will have rigidbody and we apply force
            var containerPrefab = m_lootContainer.Prefab;
            var container = containerPrefab != null ? m_lootContainer.Prefab.GetPooledInstance(pos, spawned.transform.rotation) : null;
            if (container == null)
            {
                ShootOutDirectly(spawned, pos, force);
                return;
            }
            ShootOutWithContainer(spawned, pos, container, force);
        }

        void ShootOutWithContainer(GameObject spawned, Vector3 pos, GameObject container, Vector3 force)
        {
            spawned.transform.position = pos;
            spawned.transform.SetParent(container.transform);

            if (container.TryGetComponent<Rigidbody>(out var rigidContainer))
            {
                // todo: hacky
                rigidContainer.constraints = spawned.TryGetComponent(out Rigidbody spawnedRigidbody) ? spawnedRigidbody.constraints : RigidbodyConstraints.None;
                rigidContainer.velocity= Vector3.zero;
                
                rigidContainer.AddForce(force, ForceMode.Impulse);
            }
            // once rigidbody container lands on ground and is resting (no velocity)
            // it will unparent the spawned object and destroy previous container etc.
            if (!container.TryGetComponent<UnparentAC>(out var unparentBehaviour))
                return;
            unparentBehaviour.AddTransform(spawned.transform);

            if (!container.TryGetComponent<RigidbodyOnRestEC>(out var restAction))
            {
                Debug.LogError($"No {nameof(RigidbodyOnRestEC)} on gameObject {gameObject}!");
                return;
            }

            restAction.AddOnRestListener(() => unparentBehaviour.DoUnparent());
            if (spawned.TryGetComponent<LootSpawnedEC>(out var action))
                restAction.AddOnRestListener(action.OnLootSpawned);
            restAction.enabled = true;
        }

        void ShootOutDirectly(GameObject spawned, Vector3 pos, Vector3 force)
        {
            if (!spawned.TryGetComponent<Rigidbody>(out var rigid)) return;
            rigid.position = pos;
            rigid.velocity = Vector3.zero;
            rigid.angularVelocity = Vector3.zero;
            rigid.AddForce(m_transformData.Rotation* force, ForceMode.Impulse);
        }

        void GetSpawnForce(out Vector3 force, out Vector3 offset)
        {
            force = Vector3.up * m_spawnForce.y;
            offset = Vector3.up * m_spawnPosOffset.y;

            if (Mathf.Abs(m_spawnForce.x) <= float.Epsilon)
                return;
            var angle = Random.Range(0, 360);
            var radian = angle * Mathf.Deg2Rad;
            var x = Mathf.Cos(radian);
            var z = Mathf.Sin(radian);
            var dir = new Vector3(x, 0, z);

            var forceAdd = dir * m_spawnForce.x;
            var offsetAdd = dir * m_spawnPosOffset.x;
            force += forceAdd;
            offset += offsetAdd;

            //Debug.Log($"{angle} {offset} {force}");
        }
    }
}
