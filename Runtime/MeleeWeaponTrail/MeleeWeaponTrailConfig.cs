﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using UnityEngine;


namespace GameUtility.MeleeWeaponTrail
{
    [CreateAssetMenu(menuName = "Game/WeaponTrailConfig")]
    public class MeleeWeaponTrailConfig : ScriptableObject
    {
        [Serializable]
        public struct PointsForAnimation
        {
            public int StateHash;
            public PointData[] PointDataList;
        }

        [Serializable]
        public struct PointData
        {
            public float NormalizedTime;
            public MeleeWeaponTrail.Point[] Points;
        }
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] PointsForAnimation[] m_animationPoints = new PointsForAnimation[0];
#pragma warning restore 0649 // wrong warnings for SerializeField
        struct GetPointsOperationData
        {
            public MeleeWeaponTrail.Point[] MinPPoints;
            public MeleeWeaponTrail.Point[] MaxPPoints;
            public float LerpVal;
            public int MinPLength;
            public int MaxPLength;
            public int CommonPointsLength;
        }

        public void SetPoints(int stateHash, float normalizedTime, List<MeleeWeaponTrail.Point> points, Transform parent)
        {
            var animIdx = Array.FindIndex(m_animationPoints, item => item.StateHash == stateHash);
            if (animIdx == -1)
                AddAnimationState(stateHash, out animIdx);
            var list = m_animationPoints[animIdx].PointDataList.ToList();
            ToLocalPoints(points, parent, out var pointArray);

            list.Add(new PointData(){ NormalizedTime = normalizedTime, Points = pointArray });
            
            m_animationPoints[animIdx].PointDataList = list.ToArray();
        }

        static void ToLocalPoints(List<MeleeWeaponTrail.Point> points, Transform parent, out MeleeWeaponTrail.Point[] pointArray)
        {
            pointArray = points.ToArray();
            for (var i = 0; i < pointArray.Length; i++)
            {
                var p = pointArray[i];
                p.BasePosition = parent.InverseTransformPoint(p.BasePosition);
                p.TipPosition = parent.InverseTransformPoint(p.TipPosition);
                pointArray[i] = p;
            }
        }

        void AddAnimationState(int stateHash, out int animIdx)
        {
            var list = m_animationPoints.ToList();
            list.Add(new PointsForAnimation() {StateHash = stateHash, PointDataList = new PointData[0] });
            m_animationPoints = list.ToArray();
            animIdx = m_animationPoints.Length;
        }

        public bool TryGetPoints(int stateHash, float normalizedTime, List<MeleeWeaponTrail.Point> points, Transform parent)
        {
            var animIdx = Array.FindIndex(m_animationPoints, item => item.StateHash == stateHash);
            if (animIdx == -1)
            {
                //Debug.LogWarning($"Couldn't find animation for stateHash {stateHash}");
                return false;
            }   
            var animationPoints = m_animationPoints[animIdx];
            var pointDataList = animationPoints.PointDataList;

            PrepareDataForPointLerping(normalizedTime, pointDataList,
                out var opData);

            LerpPoints(points, opData, parent);
            return true;
        }

        static void LerpPoints(ICollection<MeleeWeaponTrail.Point> points, GetPointsOperationData opData, Transform parent)
        {
            var pointIdx = 0;
            points.Clear();
            for (; pointIdx < opData.CommonPointsLength; pointIdx++)
            {
                var minP = opData.MinPPoints[pointIdx];
                var maxP = opData.MaxPPoints[pointIdx];
                var basePos = Vector3.Lerp(minP.BasePosition, maxP.BasePosition, opData.LerpVal);
                var tipPos = Vector3.Lerp(minP.TipPosition, maxP.TipPosition, opData.LerpVal);
                var time = Mathf.Lerp(minP.Time, maxP.Time,
                    opData.LerpVal);
                
                points.Add(new MeleeWeaponTrail.Point() {
                    BasePosition = parent.TransformPoint(basePos),
                    TipPosition = parent.TransformPoint(tipPos),
                    Time = time});
            }

            for (; pointIdx < opData.MinPLength; pointIdx++)
            {
                var minP = opData.MinPPoints[pointIdx];
                points.Add(new MeleeWeaponTrail.Point()
                {
                    BasePosition = parent.TransformPoint(minP.BasePosition),
                    TipPosition = parent.TransformPoint(minP.TipPosition),
                    Time = minP.Time
                });
            }

            for (; pointIdx < opData.MaxPLength; pointIdx++)
            {
                var maxP = opData.MaxPPoints[pointIdx];
                points.Add(new MeleeWeaponTrail.Point()
                {
                    BasePosition = parent.TransformPoint(maxP.BasePosition),
                    TipPosition = parent.TransformPoint(maxP.TipPosition),
                    Time = maxP.Time
                });
            }
        }

        static void PrepareDataForPointLerping(float normalizedTime, IList<PointData> pointDataList, 
            out GetPointsOperationData opData)
        {
            GetLerpingPointData(normalizedTime, pointDataList, out var minIdx, out var maxIdx);
            var minPData = pointDataList[minIdx];
            var maxPData = pointDataList[maxIdx];
            opData.MinPPoints = minPData.Points;
            opData.MaxPPoints = maxPData.Points;

            var minNorm = minPData.NormalizedTime;
            var normRange = maxPData.NormalizedTime - minNorm;
            opData.LerpVal = normRange > 0f ? normalizedTime - minIdx / normRange : 0f;

            opData.MinPLength = minPData.Points.Length;
            opData.MaxPLength = maxPData.Points.Length;

            opData.CommonPointsLength = Mathf.Min(opData.MinPLength, opData.MaxPLength);
        }

        static void GetLerpingPointData(float normalizedTime, IList<PointData> pointDataList, out int minIdx, out int maxIdx)
        {
            minIdx = 0;
            maxIdx = 0;
            for (; maxIdx < pointDataList.Count-1; maxIdx++)
            {
                if (pointDataList[maxIdx].NormalizedTime >= normalizedTime)
                    break;
                minIdx = maxIdx;
            }
        }

        public void ClearPoints(int stateHash)
        {
            var animIdx = Array.FindIndex(m_animationPoints, item => item.StateHash == stateHash);
            if (animIdx == -1)
                return;
            m_animationPoints[animIdx].PointDataList = new PointData[0];
        }

        public void ReducePoints(int stateHash, float animationLength, float pointsPerSecond)
        {
            var animIdx = Array.FindIndex(m_animationPoints, item => item.StateHash == stateHash);
            if (animIdx == -1)
                return;
            var ps = m_animationPoints[animIdx].PointDataList.ToList();
            if(ps.IsNullOrEmpty())
                return;
            var lastPointTime = ps.Last().NormalizedTime;
            for (var i = ps.Count-2; i > 0; i--)
            {
                var t = ps[i].NormalizedTime;
                var secondsPassed = (lastPointTime - t) * animationLength;
                var pointsAllowed = secondsPassed * pointsPerSecond;
                if (pointsAllowed >= 1)
                {
                    lastPointTime = t;
                    continue;
                }

                ps.RemoveAt(i);
            }

            m_animationPoints[animIdx].PointDataList = ps.ToArray();
        }
    }
}