﻿namespace GameUtility.InitSystem
{
    public interface IInitDataComponent<TData> where TData : struct
    {
        void GetData(out TData data);
        void InitializeWithData(TData data);
    }
}
